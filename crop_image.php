<?

  require_once('Classes/CropImage.php');

  $image_size_available = 30 * 1024 * 1024;
  $image_formats_available = ['png', 'jpg', 'jpeg', 'wepb'];

  $image = $_FILES['image'];
  $format = explode('/', $_FILES['image']['type'])['1'];

  $upload_folder = 'upload';
  $upload_image_name = 'image.'.$format;
  $path = $upload_folder.'/'.$upload_image_name;

  if ($image['size'] > $image_size_available) {
    // die('Limit size');
  }

  if (!in_array($format, $image_formats_available)) {
    die('Wrong format');
  }

  switch ($format) {
    case 'jpeg':

      break;
    case 'jpg':

      break;
    case 'png':

      break;
    case 'wepb':

      break;
  }
  move_uploaded_file($image['tmp_name'], $path);

  $crop = new CropImage($path);
  $images = $crop->croppingImage();
  echo json_encode($images);

?>
