<?

class CropImage {

  private $image_path;
  private $image;
  private $image_format;
  private $image_width;
  private $image_height;
  private $func_save;
  private $upload_crop_path = 'crop';

  public function __construct($image_path) {
    $this->image_path = $image_path;
    $this->image_format = end(explode('.', $this->image_path));

    switch ($this->image_format) {
      case 'jpg':
      case 'jpeg':
        $this->image = imagecreatefromjpeg($this->image_path);
        $this->func_save = 'imagejpeg';
        break;
      case 'png':
        $this->image = imagecreatefrompng($this->image_path);
        $this->func_save = 'imagepng';
        break;
      case 'wepb':
        $this->image = imagecreatefromwepb($this->image_path);
        $this->func_save = 'imagewepb';
        break;
    }

    $this->image_height = imagesy($this->image);
    $this->image_width = imagesx($this->image);
  }

  public function templateOne() {
    return [
      '0' => [
        'x' => 0,
        'y' => 30,
        'width' => 20,
        'height' => 30
      ],
      '1' => [
        'x' => 20,
        'y' => 22,
        'width' => 20,
        'height' => 56
      ],
      '2' => [
        'x' => 40,
        'y' => 0,
        'width' => 20,
        'height' => 100
      ],
      '3' => [
        'x' => 60,
        'y' => 22,
        'width' => 20,
        'height' => 56
      ],
      '4' => [
        'x' => 80,
        'y' => 30,
        'width' => 20,
        'height' => 30
      ]
    ];
  }

  public function getCoordinates($data) {
    return [
      'x' => ($this->image_width/100)*$data['x'],
      'y' => ($this->image_height/100)*$data['y'],
      'width' => ($this->image_width/100)*$data['width'],
      'height' => ($this->image_height/100)*$data['height']
    ];
  }

  public function croppingImage() {

    $template = $this->templateOne();
    $image_num = 1;
    $func_save = $this->func_save;
    $images_cropped_path = [];

    foreach ($template as $item) {
      $coordinates = $this->getCoordinates($item);
      $image_crop = imagecrop($this->image, [
        'x' => $coordinates['x'],
        'y' => $coordinates['y'],
        'width' => $coordinates['width'],
        'height' => $coordinates['height']
      ]);
      $path = $this->upload_crop_path.'/cropped_image_'.$image_num.'.'.$this->image_format;
      $func_save($image_crop, $path);

      $image_num++;
      $images_cropped_path[] = $path.'?cash='.mt_rand(0, 9999999);
    }
    imagedestroy($this->image);
    return $images_cropped_path;
  }



}

?>
