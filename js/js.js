
	$('input[name="file"]').change(function() {
		var input = $(this)[0];
            if (input.files && input.files[0]) {
                if (input.files[0].size > (30 * 1024 * 1024)) {
                    // alert('Max size 30MB');
                    // return;
                }
		var data = new FormData();
    data.append('image', input.files[0]);
		$('.label-input-file').attr('for', '');
		$.ajax({
		type: 'POST',
		url: "/crop_image.php",
		data: data,
		dataType: 'text',
		cache: false,
		contentType: false,
		processData: false,
		xhr: function()
		{
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function(e){
				if (e.lengthComputable) {
					var percent = (e.loaded / e.total) * 100;
					$('.input-file-button').text(Math.trunc(percent) + '%');
					if (Math.trunc(percent) == 100) {
						var flag = true;
						$('.input-file-button').text('Обработка');
						intervalId = setInterval(function() {
							if (flag) {
								flag = false;
								$('.input-file-button').addClass('input-file-button-loader');
							}
							else {
								flag = true;
								$('.input-file-button').removeClass('input-file-button-loader');
							}
						}, 1000)
						var reader = new FileReader();
						reader.onload = function (event) {
							$('.result-image').toggle();
        			$('.preview-image').toggle();
							$('.preview-image').html('<img src="' + event.target.result + '">');
						};
						reader.readAsDataURL(input.files[0]);
						}
				}
			}, false);
			return xhr;
		},
		success: function(data){
			data = JSON.parse(data);
			var html = '';
			for (i=0; i<data.length; i++) {
				html += '<img src="/' + data[i] + '">';
			}
			$('.result-image').html(html);
			$('.preview-image').toggle();
			$('.result-image').toggle();
			$('.input-file-button').text('Загрузить');
      $('.label-input-file').attr('for', 'input-file');
			clearInterval(intervalId);
			if ($('.input-file-button').hasClass('input-file-button-loader')) {
				$('.input-file-button').removeClass('input-file-button-loader');
			}
		}
		});
	}
	});
